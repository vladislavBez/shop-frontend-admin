﻿import React from 'react';
import Dropzone from 'react-dropzone';
import request from 'superagent';
import './index.css';

export default class InputImage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            uploadedFile: null
        };
    }

    onImageDrop(files) {
        let file = files[0];
        this.setState({
            uploadedFile: file
        });
    }

    onImageDeleteHandler() {
        this.setState({
            uploadedFile: null
        });
    }

    render() {
        
        let previewImage;

        if (this.state.uploadedFile) {
            previewImage = <div
                className="preview_img"
            >
                <div
                    className="close_upload_img"
                    onClick={this.onImageDeleteHandler.bind(this)}
                >
                    Удалить изображение
                </div>
                <img src={this.state.uploadedFile.preview} alt="product" />
            </div>
        } else {
            previewImage = <Dropzone
                className="preview_load_img"
                multiple={false}
                accept="image/*"
                onDrop={this.onImageDrop.bind(this)}>
                <p>Кликните или перетащите мышкой файл изображения для загрузки</p>
            </Dropzone>     
        }

        return (
            <div className="block_load_img">
                {previewImage}
            </div>
        );
    }
}