//Root server url
const ROOT_URL = 'http://localhost:50735'

//Urls server api administrator
export const GET_PRODUCTS_LIST_URL = ROOT_URL + '/Admin/getProductsList'
export const GET_PRODUCTS_DETAILS_URL = ROOT_URL + '/Admin/getProductDetails'
export const SET_PRODUCT_URL = ROOT_URL + '/Admin/postSetProduct'
