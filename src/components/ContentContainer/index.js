import React from 'react';
import './index.css';

export default class ContainerContent extends React.Component {
    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}