﻿import React from 'react';
import { Link } from 'react-router-dom'
import './index.css';

export default class TopMenu extends React.Component {
    render() {
        return (
            <div className="container-fluid top_section">
                <div className="row">
                    <div className="top_menu">
                        <ul>
                            <li><Link to='/products/list'>Товары</Link></li>
                            <li><Link to='/orders/list'>Заказы</Link></li>
                            <li><Link to='/users/list'>Пользователи</Link></li>
                        </ul>
                    </div>
                </div>
            </div>
            
        );
    }
}