﻿import React from 'react';
import './index.css';

export class ToolBar extends React.Component {

    render() {

        return (
            <div className="toolbar">
                {this.props.children}
            </div>
        );
    }
}

export class ToolBarRow extends React.Component {

    render() {

        return (
            <div className="toolbar-row">
                {this.props.children}
            </div>
        );
    }
}

export class ToolBarColl extends React.Component {

    render() {

        return (
            <div className="toolbar-coll">
                {this.props.children}
            </div>
        );
    }
}

export class ToolBarInputText extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value ? this.props.value : ""
        };
    }

    handleChange(event) {
        this.setState({
            value: event.target.value
        });
    }

    render() {

        let name = this.props.name ? this.props.name : "";
        let label = this.props.label ? this.props.label : "";
        let id = this.props.name ? "id" + this.props.name : "";
        let size_class;
        if (this.props.size) {
            switch (this.props.size) {
                case "0":
                    size_class = "input-full";
                    break;
                case "1":
                    size_class = "input-small";
                    break;
                case "2":
                    size_class = "input-middle";
                    break;
                default:
                    size_class = "input-full";
            }
        }
        let size = this.props.size ? "id" + this.props.name : "width";

        let input;
        if (this.props.disabled) {
            input = <div className={size_class + " toolbar-input-span toolbar-input-disabled"}>
                {this.state.value}
            </div>
        } else {
            input = <input
                className={size_class + " toolbar-input-span"}
                type="text"
                name={name}
                id={id}
                value={this.state.value}
                onChange={this.handleChange.bind(this)}
            />
        }

        return (
            <div className="toolbar-input-text">
                <label htmlFor={id}>{label}</label>
                {input}
            </div>
        );
    }
}