﻿import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as productsActions from '../actions/ProductsActions'

class Products extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        products: state.products.products,
        user: state.products.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        productsActions: bindActionCreators(productsActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products)