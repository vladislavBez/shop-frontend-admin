﻿import {
    GET_PRODUCTS_LIST_REQUEST,
    GET_PRODUCTS_LIST_SUCCESS,
    GET_PRODUCTS_LIST_FAILURE,

    GET_PRODUCTS_DETAILS_REQUEST,
    GET_PRODUCTS_DETAILS_SUCCESS,
    GET_PRODUCTS_DETAILS_FAILURE,

    SET_PRODUCT_REQUEST,
    SET_PRODUCT_SUCCESS,
    SET_PRODUCT_FAILURE,

    SET_DEFAULT_VALUES,
    SET_SELECT_PRODUCTS,
    SET_SELECT_ALL_PRODUCTS
} from '../constants/Products'

const initialState = {
    //Product
    id: "",
    image: "",
    name: "",
    price: "",
    isNewProduct: true,
    //List products
    search: "",
    priceFrom: 0,
    priceTo: 0,
    products: [],
    sortOrder: "NO_SORT",
    selectProducts: [],
    countSelectProducts: 0,
    isSelectAllProducts: false
}

export default function products(state = initialState, action) {
    console.log(action);
    switch (action.type) {

        //Select all products on table products
        case SET_SELECT_ALL_PRODUCTS:
            return {
                ...state,
                isSelectAllProducts: action.payload.isSelectAllProducts,
                countSelectProducts: action.payload.countSelectProducts,
                selectProducts: action.payload.selectProducts
            }

        //Set select products on table products
        case SET_SELECT_PRODUCTS:
            return {
                ...state,
                countSelectProducts: action.payload.countSelectProducts,
                selectProducts: action.payload.selectProducts
            }

        //Get list of products
        case GET_PRODUCTS_LIST_REQUEST:
            return {
                ...state
            }

        case GET_PRODUCTS_LIST_SUCCESS:
            return {
                ...state,
                products: action.payload.products,
                sortOrder: action.payload.sortOrder
            }

        case GET_PRODUCTS_LIST_FAILURE:
            return {
                ...state
            }

        //Get product details
        case GET_PRODUCTS_DETAILS_REQUEST:
            return {
                ...state
            }

        case GET_PRODUCTS_DETAILS_SUCCESS:
            return {
                ...state,
                isNewProduct: false,
                id: action.payload.product.id,
                image: action.payload.product.image,
                name: action.payload.product.name,
                price: action.payload.product.price
            }

        case GET_PRODUCTS_DETAILS_FAILURE:
            return {
                ...state
            }

        //Set (save) one product
        case SET_PRODUCT_REQUEST:
            return {
                ...state
            }

        case SET_PRODUCT_SUCCESS:
            return {
                id: action.payload.response.id,
                image: action.payload.response.image,
                name: action.payload.response.name,
                price: action.payload.response.price
            }

        case SET_PRODUCT_FAILURE:
            return {
                ...state
            }

        case SET_DEFAULT_VALUES:
            return {
                ...state,
                id: initialState.id,
                image: initialState.image,
                name: initialState.name,
                price: initialState.price
            }

        default:
            return state;
    }

}