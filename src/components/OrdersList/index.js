﻿import React from 'react';
import './index.css';
import { ButtonToolbar } from '../../shared/Button'
import { ToolBar, ToolBarRow } from '../../shared/ToolBar'

export default class OrdersList extends React.Component {
    render() {
        return (
            <div className="container-fluid">

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <div className="header_gray">Все заказы</div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <ToolBar>
                            <ToolBarRow>
                                <ButtonToolbar 
                                    value="Создать"
                                    linkTo='/orders/add'
                                />
                                <ButtonToolbar 
                                    value="Редактировать"
                                    linkTo='/products/edit'
                                />
                                <ButtonToolbar 
                                    value="Удалить"
                                />
                                <ButtonToolbar 
                                    value="Закрыть"
                                    linkTo='/'
                                    pullRight={true}
                                />
                            </ToolBarRow>
                        </ToolBar>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <table className="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Изображение</th>
                                    <th>Цена</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>1</th>
                                    <th>
                                        <div className="cart_img">
                                            <img src="../images/products/Harley-Davidson Dyna Wide Glide.jpg" alt="product"/>
                                        </div>
                                    </th>
                                    <th>500 000 р</th>
                                    <th><span className="light-gray">Убрать из корзины</span></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        );
    }
}