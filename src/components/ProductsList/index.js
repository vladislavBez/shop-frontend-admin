﻿import React from 'react';
import './index.css';
import { ButtonToolbar } from '../../shared/Button'
import { ToolBar, ToolBarRow, ToolBarColl, ToolBarInputText } from '../../shared/ToolBar'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as productsActions from '../../actions/ProductsActions'
import InputText from '../../shared/InputText';
import { getIndexOfById } from '../../services/arrayServices'

const NO_SORT = "NO_SORT"
const NAME = "NAME"
const NAME_DESC = "NAME_DESC"
const PRICE = "PRICE"
const PRICE_DESC = "PRICE_DESC"
const ID = "ID"
const ID_DESC = "ID_DESC"

const ASC_LABEL = "(Сортировка по возрастанию)"
const DESC_LABEL = "(Сортировка по убыванию)"

class ProductsList extends React.Component {

    componentDidMount() {
        this.getProductsList(NO_SORT);
    }

    getProductsList(sortOrder) {
        const search = this.refs.search.state.value;
        const priceFrom = Number(this.refs.priceFrom.state.value);
        const priceTo = Number(this.refs.priceTo.state.value);
        if ((priceFrom >= 0) && (priceTo >= 0) && (priceFrom <= priceTo)) {
            this.props.productsActions.getProductsList(search, priceFrom, priceTo, sortOrder);
        } else {
            alert("Пожалуйста, установите корректные значения");
        }
    }

    onSearchHandler() {
        this.getProductsList(NO_SORT);
    }

    onSortOrderHandler(sortOrder) {
        let oldSort = this.props.sortOrder;
        let newSort;

        switch (sortOrder) {
            case NAME:
                if (oldSort === NAME) {
                    newSort = NAME_DESC;
                    break;
                } else {
                    newSort = NAME;
                    break;
                }
            case PRICE:
                if (oldSort === PRICE) {
                    newSort = PRICE_DESC;
                    break;
                } else {
                    newSort = PRICE;
                    break;
                }
            case ID:
                if (oldSort === ID) {
                    newSort = ID_DESC;
                    break;
                } else {
                    newSort = ID;
                    break;
                }
            default:
                newSort = NO_SORT;
        }
        this.getProductsList(newSort);
    }

    onCheckHandler(index) {
        const products = this.props.products;
        const selectProducts = this.props.selectProducts;
        this.props.productsActions.setSelectProducts(products, index, selectProducts);
    }

    onCheckAllHandler() {
        const products = this.props.products;
        const isSelectAllProducts = this.props.isSelectAllProducts;
        this.props.productsActions.setSelectAllProducts(products, isSelectAllProducts);
    }

    onEditHandler() {
        let selectProducts = this.props.selectProducts;
        if (selectProducts.length === 1) {
            this.props.history.push("/products/edit/" + selectProducts[0].id);
        } else {
            alert("Пожалуйста, выберите один товар для редактирования");
        }
    }

    onRemoveHandler() {

    }

    render() {
        let products = this.props.products;
        let selectProducts = this.props.selectProducts;
        let isSelectAllProducts = this.props.isSelectAllProducts;
        let table;

        let sortOrder = this.props.sortOrder;
        let nameHeader = "Название",
            priceHeader = "Цена",
            idHeader = "id";

        switch (sortOrder) {
            case NAME:
                nameHeader = nameHeader + ASC_LABEL;
                break;
            case NAME_DESC:
                nameHeader = nameHeader + DESC_LABEL;
                break;
            case PRICE:
                priceHeader = priceHeader + ASC_LABEL;
                break;
            case PRICE_DESC:
                priceHeader = priceHeader + DESC_LABEL;
                break;
            case ID:
                idHeader = idHeader + ASC_LABEL;
                break;
            case ID_DESC:
                idHeader = idHeader + DESC_LABEL;
                break;
            default:
                break;
        }

        if (products.length === 0) {
            table = <div className="empty-content">
                По заданным значениям фильтров данных не найдено
            </div>
        } else {
            table = <table className="table table-striped table-hover table-products">
                <thead>
                    <tr>
                        <th>
                            <input
                                type="checkbox"
                                onClick={this.onCheckAllHandler.bind(this)}
                                checked={isSelectAllProducts}
                                readOnly={true}
                            />
                        </th>
                        <th>Изображение</th>
                        <th
                            className="header-order"
                            onClick={this.onSortOrderHandler.bind(this, NAME)}
                        >
                            {nameHeader}
                        </th>
                        <th
                            className="header-order"
                            onClick={this.onSortOrderHandler.bind(this, PRICE)}
                        >
                            {priceHeader}
                        </th>
                        <th
                            className="header-order"
                            onClick={this.onSortOrderHandler.bind(this, ID)}
                        >
                            {idHeader}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {products.map((product, i) =>
                        <tr
                            id={"table_row" + i}
                            key={i}
                            onClick={this.onCheckHandler.bind(this, i)}
                            className={getIndexOfById(selectProducts, product) != - 1 ? "info" : ""}
                        >
                            <th>
                                <input
                                    id={"checkbox" + i}
                                    type="checkbox"
                                    checked={getIndexOfById(selectProducts, product) != -1}
                                    readOnly={true}
                                />
                            </th>
                            <th>
                                <div className="cart_img">
                                    <img src="" alt="product" />
                                </div>
                            </th>
                            <th>{product.name}</th>
                            <th>{product.price}</th>
                            <th>{product.id}</th>
                        </tr>
                    )}
                </tbody>
            </table>
        }

        return (
            <div className="container-fluid">

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <div className="header_gray">Все товары</div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <ToolBar>
                            <ToolBarRow>
                                <ButtonToolbar
                                    value="Создать"
                                    linkTo='/products/add'
                                />
                                <ButtonToolbar 
                                    value="Редактировать"
                                    onClick={this.onEditHandler.bind(this)}
                                />
                                <ButtonToolbar 
                                    value="Удалить"
                                    onClick={this.onRemoveHandler.bind(this)}
                                />
                                <ButtonToolbar 
                                    value="Закрыть"
                                    linkTo='/'
                                    pullRight={true}
                                />
                            </ToolBarRow>
                            <ToolBarRow>
                                <ToolBarColl>
                                    <ToolBarInputText
                                        ref="search"
                                        name="search"
                                        label="Поиск по названию"
                                        size="2"
                                    />
                                </ToolBarColl>
                                <ToolBarColl>
                                    <ToolBarInputText
                                        ref="priceFrom"
                                        name="priceFrom"
                                        label="Цена от"
                                        size="1"
                                    />
                                    <ToolBarInputText
                                        ref="priceTo"
                                        name="priceTo"
                                        label="до"
                                        size="1"
                                    />
                                </ToolBarColl>
                                <ToolBarColl>
                                    <ButtonToolbar
                                        value="Найти"
                                        onClick={this.onSearchHandler.bind(this)}
                                    />
                                </ToolBarColl>
                            </ToolBarRow>
                        </ToolBar>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">                       
                        {table}
                    </div>
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        products: state.products.products,
        sortOrder: state.products.sortOrder,
        selectProducts: state.products.selectProducts,
        countSelectProducts: state.products.countSelectProducts,
        isSelectAllProducts: state.products.isSelectAllProducts
    }
}

function mapDispatchToProps(dispatch) {
    return {
        productsActions: bindActionCreators(productsActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductsList)