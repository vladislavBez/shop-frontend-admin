﻿import React from 'react';
import TopMenu from '../../components/TopMenu';
import ContentContainer from '../../components/ContentContainer';
import './index.css';

export default class Home extends React.Component {
    
    render() {
        return (
            <div>
                <TopMenu />
                <ContentContainer />
            </div>
        );
    }
}