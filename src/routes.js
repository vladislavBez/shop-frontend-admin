import React from 'react';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter } from 'react-router-redux'

import HomeAdmin from './views/Home';

import Products from './containers/Products';
import ProductsList from './components/ProductsList';
import ProductEdit from './components/ProductEdit';

import Orders from './containers/Orders';
import OrdersList from './components/OrdersList';

import Users from './containers/Users';
import UsersList from './components/UsersList';

const history = createHistory()

export default () => (
    <ConnectedRouter history={history}>
        <div>
            <Route path="/" component={HomeAdmin} />

            <Route path="/products" component={Products} />
            <Route path="/products/list" component={ProductsList} />
            <Route path="/products/add" component={ProductEdit} />
            <Route path="/products/edit/:id" component={ProductEdit} />
            
            <Route path="/orders" component={Orders} />
            <Route path="/orders/list" component={OrdersList} />

            <Route path="/users" component={Users} />
            <Route path="/users/list" component={UsersList} />

        </div>
    </ConnectedRouter>
);