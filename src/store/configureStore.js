import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from '../reducers'
import thunk from 'redux-thunk'
import { routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

export default function configureStore(initialState) {

    const history = createHistory()
    const reduxRouterMiddleware = routerMiddleware(history);
    const middlewares = [
        reduxRouterMiddleware,
        thunk,
    ];
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(
        rootReducer,
        initialState,
        composeEnhancers(applyMiddleware(...middlewares))
    )

    return store
}