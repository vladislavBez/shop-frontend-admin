//For products
export function getIndexOfById(array, element) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].id === element.id) {
            return i;
        }
    }
    return -1;
}