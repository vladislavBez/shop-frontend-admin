﻿import React from 'react';
import InputText from '../../shared/InputText';
import InputImage from '../../shared/InputImage';
import './index.css';
import { ButtonToolbar } from '../../shared/Button'
import { ToolBar, ToolBarRow } from '../../shared/ToolBar'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as productsActions from '../../actions/ProductsActions'

class ProductEdit extends React.Component {
    
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.productsActions.getProductsDetails(this.props.match.params.id);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.id != "") {
            this.refs.idProduct.state.value = nextProps.id;
        }
        if (!nextProps.isNewProduct) {
            this.refs.idProduct.setValue(nextProps.id);
            this.refs.nameProduct.setValue(nextProps.name);
            this.refs.priceProduct.setValue(nextProps.price);
        }
    }

    onSaveHandler() {
        const id = this.refs.idProduct.state.value,
            name = this.refs.nameProduct.state.value,
            price = this.refs.priceProduct.state.value,
            image = this.refs.imageProduct.state.uploadedFile;
        if ((name == "") || (price == "") || (image == null)) {
            alert("Пожалуйста, заполните все поля");
        } else {
            this.props.productsActions.setProduct(id, name, price, image);
        }
    }

    onCloseHandler() {
        this.props.productsActions.setDefaultValues();
        this.props.history.push("/products/list");
    }

    render() {

        let id = this.props.id;
        let image = this.props.image;
        let name = this.props.name;
        let price = this.props.price;
        let isNewProduct = this.props.isNewProduct;

        return (
            <div className="container-fluid">

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <div className="header_gray">Создать товар</div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <ToolBar onClick={this.onSaveHandler.bind(this)}>
                            <ToolBarRow>
                                <ButtonToolbar 
                                    value="Сохранить"
                                    onClick={this.onSaveHandler.bind(this)}
                                />
                                <ButtonToolbar 
                                    value="Закрыть"
                                    pullRight={true}
                                    onClick={this.onCloseHandler.bind(this)}
                                />
                            </ToolBarRow>
                        </ToolBar>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-6 col-lg-6">
                        <div className="container_product_load_img">
                            <InputImage
                                ref="imageProduct"
                            />
                        </div>
                    </div>

                    <div className="col-md-6 col-lg-6">
                        <div className="row">
                            <div className="col-md-12 col-lg-12">
                                <InputText
                                    ref="nameProduct"
                                    name="nameProduct"
                                    label="Название"
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 col-lg-12">
                                <InputText
                                    ref="priceProduct"
                                    name="priceProduct"
                                    label="Цена"
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 col-lg-12">
                                <InputText
                                    ref="idProduct"
                                    name="idProduct"
                                    label="id товара"
                                    disabled={true}
                                />
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        id: state.products.id,
        image: state.products.image,
        name: state.products.name,
        price: state.products.price,
        isNewProduct: state.products.isNewProduct
    }
}

function mapDispatchToProps(dispatch) {
    return {
        productsActions: bindActionCreators(productsActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductEdit)