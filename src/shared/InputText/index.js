﻿import React from 'react';
import './index.css';

export default class InputText extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value ? this.props.value : ""
        };
    }

    handleChange(event) {
        this.setState({
            value: event.target.value
        });
    }

    setValue(value) {
        this.setState({
            value: value
        });
    }

    render() {

        let name = this.props.name ? this.props.name : "";
        let label = this.props.label ? this.props.label : "";
        let id = this.props.name ? "id" + this.props.name : "";
        
        let input;
        if (this.props.disabled) {
            input = <div className="input-span input-disabled">
                {this.state.value}
            </div>
        } else {
            input = <input
                className="input-span"
                type="text"
                name={name}
                id={id}
                value={this.state.value}
                onChange={this.handleChange.bind(this)}
            />
        }

        return (
            <div className="input-text">
                <label htmlFor={id}>{label}</label>
                {input}
            </div>
        );
    }
}