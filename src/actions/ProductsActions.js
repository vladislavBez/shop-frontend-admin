import {
    GET_PRODUCTS_LIST_REQUEST,
    GET_PRODUCTS_LIST_SUCCESS,
    GET_PRODUCTS_LIST_FAILURE,

    GET_PRODUCTS_DETAILS_REQUEST,
    GET_PRODUCTS_DETAILS_SUCCESS,
    GET_PRODUCTS_DETAILS_FAILURE,

    SET_PRODUCT_REQUEST,
    SET_PRODUCT_SUCCESS,
    SET_PRODUCT_FAILURE,

    SET_DEFAULT_VALUES,
    SET_SELECT_PRODUCTS,
    SET_SELECT_ALL_PRODUCTS
} from '../constants/Products'

import {
    GET_PRODUCTS_LIST_URL,
    SET_PRODUCT_URL,
    GET_PRODUCTS_DETAILS_URL
} from '../constants/CommonUrl'

import axios from "axios";
import { reduxForm } from 'redux-form'
import { getIndexOfById } from '../services/arrayServices'

//Get list all products
export function getProductsList(search, priceFrom, priceTo, sortOrder) {

    return (dispatch) => {
        dispatch({
            type: GET_PRODUCTS_LIST_REQUEST
        })

        axios.get(GET_PRODUCTS_LIST_URL, {
            params: {
                search: search,
                priceFrom: priceFrom,
                priceTo: priceTo,
                sortOrder: sortOrder
            }
        })
            .then((response) => {
                dispatch({
                    type: GET_PRODUCTS_LIST_SUCCESS,
                    payload: {
                        products: response.data.products,
                        sortOrder: sortOrder
                    }
                });
            })
            .catch((err) => {
                dispatch({
                    type: GET_PRODUCTS_LIST_FAILURE,
                    payload: err
                })
            })
    }
}

//Get list all products
export function getProductsDetails(id) {

    return (dispatch) => {
        dispatch({
            type: GET_PRODUCTS_DETAILS_REQUEST
        })

        axios.get(GET_PRODUCTS_DETAILS_URL, {
            params: {
                id: id
            }
        })
            .then((response) => {
                dispatch({
                    type: GET_PRODUCTS_DETAILS_SUCCESS,
                    payload: {
                        product: response.data
                    }
                });
            })
            .catch((err) => {
                dispatch({
                    type: GET_PRODUCTS_DETAILS_FAILURE,
                    payload: err
                })
            })
    }
}

//Save one product
export function setProduct(id, name, price, image) {

    let formData = new FormData();
    formData.append("id", id);
    formData.append("name", name);
    formData.append("price", price);
    formData.append("image", image);

    return (dispatch) => {
        dispatch({
            type: SET_PRODUCT_REQUEST
        })

        axios.post(SET_PRODUCT_URL, formData)
            .then((response) => {
                dispatch({
                    type: SET_PRODUCT_SUCCESS,
                    payload: {
                        response: response.data
                    }
                });
            })
            .catch((err) => {
                dispatch({
                    type: SET_PRODUCT_FAILURE,
                    payload: err
                })
            })   
    }
}

//Set default values all inputs
export function setDefaultValues() {

    return (dispatch) => {
        dispatch({
            type: SET_DEFAULT_VALUES
        })
    }
}

//Set select products on table products
export function setSelectProducts(products, index, selectProducts) {

    if (getIndexOfById(selectProducts, products[index]) === -1) {
        selectProducts[selectProducts.length] = products[index];
    } else {
        selectProducts.splice(getIndexOfById(selectProducts, products[index]), 1);
    }

    return (dispatch) => {
        dispatch({
            type: SET_SELECT_PRODUCTS,
            payload: {
                countSelectProducts: selectProducts.length,
                selectProducts: selectProducts
            }
        })
    }
}

//Select all products on table products
export function setSelectAllProducts(products, isSelectAllProducts) {

    let selectProducts = [];
    if (!isSelectAllProducts) {
        selectProducts = products.slice(0);
    }
    isSelectAllProducts = !isSelectAllProducts;

    return (dispatch) => {
        dispatch({
            type: SET_SELECT_ALL_PRODUCTS,
            payload: {
                isSelectAllProducts: isSelectAllProducts,
                countSelectProducts: selectProducts.length,
                selectProducts: selectProducts
            }
        })
    }
}
