﻿import { GET_PRODUCTS_LIST_REQUEST } from '../constants/Products'

const initialState = {
    prod: "prod 777",
    users: "Person"
}

export default function users(state = initialState, action) {

    switch (action.type) {
        case GET_PRODUCTS_LIST_REQUEST:
            console.log("GET_PRODUCTS_LIST");
            return {
                prod: action.payload
            }

        default:
            return state;
    }

}