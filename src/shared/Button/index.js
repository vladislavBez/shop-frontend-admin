﻿import React from 'react';
import './index.css';
import { Link } from 'react-router-dom'
import classNames from 'classnames';

export default class Button extends React.Component {

    render() {

        var button;

        if (this.props.linkTo) {
            button = <Link to={this.props.linkTo}>
                <button>
                    {this.props.value}
                </button>
            </Link>
        } else {
            button = <button
                onClick={this.props.onClick}
            >
                {this.props.value}
            </button>
       }

        return (
            <span>
                {button}
            </span>
        );
    }
}

export class ButtonToolbar extends React.Component {

    render() {

        var buttonClassStyle,
            button;
        
        buttonClassStyle = classNames({ 
            'button-toolbar': true,
            'button-toolbar-right': this.props.pullRight
        })

        if (this.props.linkTo) {
            button = <Link to={this.props.linkTo}>
                <button className={buttonClassStyle}>
                    {this.props.value}
                </button>
            </Link>
        } else {
            button = <button
                className={buttonClassStyle}
                onClick={this.props.onClick}
            >
                {this.props.value}
            </button>
       }

        return (
            <span>
                {button}
            </span>
        );
    }
}
